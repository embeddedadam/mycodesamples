from atlassian import Bitbucket
import argparse
import os


parser = argparse.ArgumentParser(description="Creating new repository")
parser.add_argument("Name", type=str, help="Your repository name")
parser.add_argument("Path", type=str, nargs='?', help="Path where you want to put your project in a filesystem")
parser.add_argument("Email", type=str, help="Your bitbucket Email")
parser.add_argument("Password", type=str, help="Your bitbucket password")
args = parser.parse_args()
USERNAME = args.Email
PASSWORD = args.Password
NAME = args.Name

if args.Path is not None:
    if os.path.isdir(args.Path):
        PATH = args.Path
    else:
        raise ValueError("Given path is invalid")
else:
    PATH = "/C/Users/Adaś/Desktop/REPOZYTORIA"

bitbucket = Bitbucket(
    url='https://bitbucket.org/dashboard/overview',
    username=USERNAME,
    password=PASSWORD)

a=bitbucket.repo_list("PROJ")
print(str(a))