from PyQt5.QtCore import pyqtSignal
from PyQt5.QtWidgets import QSlider
from PyQt5.uic.properties import QtWidgets


class DoubleSlider(QSlider):

    # create our our signal that we can connect to if necessary
    doubleValueChanged = pyqtSignal(float)

    def __init__(self, decimals=3, *args, **kargs):
        super(DoubleSlider, self).__init__( *args, **kargs)
        self._multi = 10 ** decimals
        self.setRange(-500, 500)
        self.setTickPosition(QSlider.TicksBothSides)
        self.setTickInterval(50)
        self.setSingleStep(5)
        self.valueChanged.connect(self.emitDoubleIntValueChanged)

    def emitDoubleIntValueChanged(self):
        int_value = int((super(DoubleSlider, self).value()) / self._multi)
        self.doubleValueChanged.emit(int_value)

    def setIntValue(self):
        self.setValue(int((super(DoubleSlider, self).value()) / self._multi))

    def value(self):
        return float(super(DoubleSlider, self).value()) / self._multi

    def setMinimum(self, value):
        return super(DoubleSlider, self).setMinimum(value * self._multi)

    def setMaximum(self, value):
        return super(DoubleSlider, self).setMaximum(value * self._multi)

    def setSingleStep(self, value):
        return super(DoubleSlider, self).setSingleStep(value * self._multi)

    def singleStep(self):
        return float(super(DoubleSlider, self).singleStep()) / self._multi

    def setValue(self, value):
        super(DoubleSlider, self).setValue(int(value * self._multi))