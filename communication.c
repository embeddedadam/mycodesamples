/*
 * Commands.c
 *
 *  Created on: 03.03.2020
 *      Author: adam.galecki
 */

#include <Communication/commands.h>
#include <MANAGER/queue.h>
#include "Communication/Frame/frame.h"
#include <stdint.h>
#include <string.h>

#ifndef NELEMS
#define NELEMS(a) (sizeof(a) / sizeof(a[0]))
#endif

#define REQUEST_SIZE 256


#pragma pack(1)

typedef struct
{
	uint8_t flag;
}reqBreakStepData_t;

_Static_assert(sizeof(reqBreakStepData_t) == sizeof(uint8_t),"error");

typedef struct
{
    QUEUE_flag_t flag;
}reqBasePositionData_t;

_Static_assert(sizeof(reqBasePositionData_t) == sizeof(uint8_t),"error");

typedef struct
{
	uint8_t stepNumber;
}reqResultPerStepData_t;

_Static_assert(sizeof(reqResultPerStepData_t) == sizeof(uint8_t),"error");

#pragma pack()



static FRAME_Status_t reqStartTest(uint8_t *data, uint16_t *dataSize);
static FRAME_Status_t reqClearList(uint8_t *data, uint16_t *dataSize);
static FRAME_Status_t reqBreakStep(uint8_t *data, uint16_t *dataSize);
static FRAME_Status_t reqRotationInTime(uint8_t *data, uint16_t *dataSize);
static FRAME_Status_t reqRotationDegrees(uint8_t *data, uint16_t *dataSize);
static FRAME_Status_t reqBasePosition(uint8_t *data, uint16_t *dataSize);
static FRAME_Status_t reqResultPerStep(uint8_t *data, uint16_t *dataSize);
static FRAME_Status_t reqTestStatus(uint8_t *data, uint16_t *dataSize);
static FRAME_Status_t reqGeneralData(uint8_t *data, uint16_t *dataSize);
static FRAME_Status_t reqClearGeneralData(uint8_t *data, uint16_t *dataSize);
static FRAME_Status_t queueStatusParser(QUEUE_commandStat_t queueStatus);
static bool breakAll(uint8_t flag);


static FRAME_Status_t (*const cmdFunctions[REQUEST_SIZE])( uint8_t *, uint16_t *) =
{
		[0x01]  = reqStartTest,
		[0x02]  = reqClearList,
		[0x04]	= reqBreakStep,
		[0x05]	= reqRotationInTime,
		[0x06]	= reqRotationDegrees,
		[0x07]  = reqBasePosition,
		[0x08]  = reqResultPerStep,
		[0x09]  = reqTestStatus,
		[0x0A]  = reqGeneralData,
		[0x0B]  = reqClearGeneralData
};

void COMMANDS_init(void)
{
	FRAME_init(cmdFunctions);
}

static FRAME_Status_t reqStartTest(uint8_t *data, uint16_t *dataSize)
{
	if(*dataSize > 0)
	{
		return FRAME_Status_ErrData;
	}
	*dataSize = 0;

	return queueStatusParser(QUEUE_start());
}

static FRAME_Status_t reqClearList(uint8_t *data, uint16_t *dataSize)
{
	if(*dataSize > 0)
	{
		return FRAME_Status_ErrData;
	}
	*dataSize = 0;

	return queueStatusParser(QUEUE_clearList());
}

static FRAME_Status_t reqBreakStep(uint8_t *data, uint16_t *dataSize)
{
	reqBreakStepData_t *reqBreak = (reqBreakStepData_t*)data;
	if(*dataSize != sizeof(reqBreakStepData_t))
	{
		return FRAME_Status_ErrData;
	}
	*dataSize = 0;

	QUEUE_break(breakAll(reqBreak->flag), false);

	return FRAME_Status_Ok;
}

static bool breakAll(uint8_t flag)
{
    if(flag == 0)
    {
        return true;
    }
    else
    {
        return false;
    }
}

static FRAME_Status_t reqRotationInTime(uint8_t *data, uint16_t *dataSize)
{
	QUEUE_reqRotationInTimeData_t *reqRotationTime = (QUEUE_reqRotationInTimeData_t*)data;
	if(*dataSize != sizeof(QUEUE_reqRotationInTimeData_t))
	{
		return FRAME_Status_ErrData;
	}
	*dataSize = 0;

	QUEUE_commandStat_t queueStat = QUEUE_insertStep(QUEUE_step_Rotation,
	        (QUEUE_stepProperties_t)*reqRotationTime);

	return queueStatusParser(queueStat);
}

static FRAME_Status_t reqRotationDegrees(uint8_t *data, uint16_t *dataSize)
{
	QUEUE_reqRotationDegreesData_t *reqRotationDegrees = (QUEUE_reqRotationDegreesData_t*)data;

	if(*dataSize != sizeof(QUEUE_reqRotationDegreesData_t))
	{
		return FRAME_Status_ErrData;
	}
	*dataSize = 0;

    QUEUE_commandStat_t queueStat = QUEUE_insertStep(QUEUE_step_Position,
            (QUEUE_stepProperties_t)*reqRotationDegrees);

    return queueStatusParser(queueStat);
}

static FRAME_Status_t reqBasePosition(uint8_t *data, uint16_t *dataSize)
{
	reqBasePositionData_t *reqBasePosition = (reqBasePositionData_t*)data;
#warning "dokonczyc gdy bedzie zaimplementowany modul kolejkujaco-licz�cy"
	if(*dataSize != sizeof(reqBasePositionData_t))
	{
		return FRAME_Status_ErrData;
	}
	*dataSize = 0;
	return FRAME_Status_Ok;
}

static FRAME_Status_t reqResultPerStep(uint8_t *data, uint16_t *dataSize)
{
    if(*dataSize != sizeof(reqResultPerStepData_t))
    {
        return FRAME_Status_ErrData;
    }

    reqResultPerStepData_t *reqResultPerStep = (reqResultPerStepData_t*)data;
	QUEUE_respResultPerStepData_t respResultData;
	QUEUE_commandStat_t logStat = QUEUE_getStepLog(
	                            reqResultPerStep->stepNumber, &respResultData);

	memcpy(data, &respResultData, sizeof(respResultData));
	*dataSize = sizeof(respResultData);
	return queueStatusParser(logStat);
}

static FRAME_Status_t reqTestStatus(uint8_t *data, uint16_t *dataSize)
{
   	if(*dataSize > 0)
	{
		return FRAME_Status_ErrData;
	}

	QUEUE_respTestStatusData_t deviceData;
	QUEUE_commandStat_t status = QUEUE_getDeviceStatus(&deviceData);
	memcpy(data, &deviceData, sizeof(deviceData));
	*dataSize = sizeof(deviceData);
	return queueStatusParser(status);
}

static FRAME_Status_t reqGeneralData(uint8_t *data, uint16_t *dataSize)
{
	if(*dataSize > 0)
	{
		return FRAME_Status_ErrData;
	}

	QUEUE_respGeneralData_t respGeneralData;
	QUEUE_commandStat_t responseStatus = QUEUE_getCurrentRotation(&respGeneralData);
	memcpy(data, &respGeneralData, sizeof(respGeneralData));
	*dataSize = sizeof(respGeneralData);
	return queueStatusParser(responseStatus);
}

static FRAME_Status_t reqClearGeneralData(uint8_t *data, uint16_t *dataSize)
{
	if(*dataSize > 0)
	{
		return FRAME_Status_ErrData;
	}
	QUEUE_commandStat_t responseStatus = QUEUE_clearRotation();
	*dataSize = 0;
	return queueStatusParser(responseStatus);
}

static FRAME_Status_t queueStatusParser(QUEUE_commandStat_t queueStatus)
{
    switch(queueStatus)
    {
    case QUEUE_stat_Ok:
        return FRAME_Status_Ok;
    case QUEUE_stat_TooManySteps:
        return FRAME_Status_ErrLimit;
    case QUEUE_stat_MotorBusy:
        return FRAME_Status_Busy;
    case QUEUE_stat_ListClear:
        return FRAME_Status_listIsClear;
    case QUEUE_stat_DataNotRead:
        return FRAME_STATUS_DataNotRead;
    case QUEUE_stat_NoValidData:
        return FRAME_Status_ErrLimit;
    case QUEUE_stat_ListIsNotClear:
        return FRAME_STATUS_listIsNotClear;
    case QUEUE_stat_Error:
    default:
        return FRAME_Status_ErrData;
    }
    return FRAME_Status_ErrData;
}
